<?php


namespace Blog\Service;

use Blog\Classes\Twig;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class SendMail
{
    private Swift_SmtpTransport $transport;
    private Swift_Mailer $mailer;
    private Twig $twig;
    private array $config;

    public function __construct()
    {
        $this->config =yaml_parse_file(__DIR__ . '../../../config/parameters.yml');
        $this->transport = new Swift_SmtpTransport(
            $this->config['mail']['host'],
            $this->config['mail']['port'],
            $this->config['mail']['encryption']
        );
        $this->transport->setUsername($this->config['mail']['username'])
        ->setPassword($this->config['mail']['password']);
        $this->mailer = new Swift_Mailer($this->transport);
        $this->twig = new Twig();
    }

    public function getAdminMail() : string
    {
        return $this->config['site']['site_admin_mail'];
    }

    public function getAdminName() : string
    {
        return $this->config['site']['site_admin_name'];
    }



    public function sendEmail(string $subject, string $from, string $name, string $mailto, string $template, array $param)
    {
        $sent = null;
        $message=null;
        $body = $this->twig->renderView($template, $param);
        if ($body != '') {
            $message = (new Swift_Message($subject))
                ->setFrom($from, $name)
                ->setTo($mailto)
                ->setBody($body, 'text/html');
            $message->addPart($body, 'text/plain');
            $sent= $this->mailer->send($message);
        }
        return $sent;
    }
}
