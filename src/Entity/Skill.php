<?php


namespace Blog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * class Skill
 * @package Blog\Entity
 * @ORM\Table(name="skill")
 * @ORM\Entity
 */
class Skill
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $skillId;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $value;

    public function setDefault($name, $value)
    {
        $this->name =$name;
        $this->value =$value;
    }

    public function getId(): int
    {
        return $this->skillId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Skill
    {
        $this->name = $name;
        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): Skill
    {
        $this->value = $value;
        return $this;
    }
}
