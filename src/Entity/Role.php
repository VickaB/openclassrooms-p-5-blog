<?php
namespace Blog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Role
 * @package Blog\Entity
 * @ORM\Table(name="role")
 * @ORM\Entity
 */
class Role
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $roleId;

    /**
     * @ORM\Column(name="label", type="string", length=45, nullable=true)
     */
    private ?string $label;

    public function getId(): int
    {
        return $this->roleId;
    }

    public function setLabel(?string $label): Role
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }
}
