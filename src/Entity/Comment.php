<?php
namespace Blog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
* Class Comment
* @package Blog\Entity
* @ORM\Table(name="comment")
* @ORM\Entity
*/
class Comment
{

    const STATUS_PENDING = 'En Attente';
    const STATUS_VALID = 'Validé';
    const STATUS_DISMISS = 'Refusé';
    const STATUS_DELETED = 'Supprimé';

    /**
    * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="IDENTITY")
    */
    private int $commentId;


    /**
    * @ORM\Column(name="content", type="text", length=65535, nullable=true)
    */
    private ?string $content = null;

    /**
    * @ORM\Column(name="date_time", type="datetime", nullable=true)
    */
    private ?DateTime $dateTime;

    /**
    * @ORM\Column(name="status", type="string", length=0, nullable=false)
    */
    private string $status;

    /**
    * @ORM\ManyToOne(targetEntity="Comment", inversedBy="children")
    */
    private ?Comment $parent;

    /**
    * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent", orphanRemoval=true, cascade={"all"})
    */
    private ?Collection $children;

    /**
    * @ORM\ManyToOne(targetEntity="Post", inversedBy="comments")
    */
    private ?Post $post;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
    */
    private User $user;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->dateTime = new DateTime();
        $this->status = self::STATUS_PENDING;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(Post $post) : Comment
    {
        $this->post = $post;
        return $this;
    }

    public function getId(): int
    {
        return $this->commentId;
    }

    public function setContent(?string $content) : Comment
    {
        $this->content = $content;

        return $this;
    }

    public function getContent() : ?string
    {
        return $this->content;
    }

    public function setDateTime(?DateTime $dateTime) : Comment
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getDateTime() : ?DateTime
    {
        return $this->dateTime;
    }


    public function setStatus(string $status) : Comment
    {
        if (!in_array(
            $status,
            array(self::STATUS_PENDING, self::STATUS_VALID, self::STATUS_DISMISS, self::STATUS_DELETED)
        )) {
            throw new InvalidArgumentException("Statut invalide");
        }
        $this->status = $status;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getParent(): ?Comment
    {
        return $this->parent;
    }

    public function setParent(?Comment $parent): Comment
    {
        $this->parent = $parent;
        return $this;
    }

    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    public function addChildren(?Comment $child): Comment
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
        }
        return $this;
    }

    public function removeComment(?Comment $child): Comment
    {
        if ($this->children->contains($child)) {
            $this->children->remove($child) ;
        }
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Comment
    {
        $this->user = $user;
        return $this;
    }
}
