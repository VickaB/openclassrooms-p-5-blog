<?php
namespace Blog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * class Image
 * @package Blog\Entity
 * @ORM\Table(name="image")
 * @ORM\Entity
 */
class Image
{
    /**
         * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private int $imageId;

    /**
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(name="highlight", type="boolean",nullable=true)
     */
    private ?bool $highlight;

    /**
     * @ORM\Column(name="path", type="string", length=45, nullable=true)
     */
    private ?string $path;

//    /**
//     * @ORM\Column(name="thumb", type="string", length=45, nullable=true)
//     */
//    private ?string $thumb;
//
//    /**
//     * @ORM\Column(name="small", type="string", length=45, nullable=true)
//     */
//    private ?string $small;
//
//    /**
//     * @ORM\Column(name="medium", type="string", length=45, nullable=true)
//     */
//    private ?string $medium;
//
//    /**
//     * @ORM\Column(name="large", type="string", length=45, nullable=true)
//     */
//    private ?string $large;

    /**
     * @ORM\ManyToOne( targetEntity="Post", inversedBy="images"))
     */
    private Post $post;

    /**
     * @ORM\ManyToOne( targetEntity="User", inversedBy="images"))
     */
    private User $user;


    public function getId(): int
    {
        return $this->imageId;
    }

    public function setName(?string $name): Image
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setPath(?string $path): Image
    {
        $this->path = $path;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function isHighlight(): ?bool
    {
        return $this->highlight;
    }

    public function setHighlight(?bool $highlight): Image
    {
        $this->highlight = $highlight;
        return $this;
    }

//    public function setThumb(?string $thumb): Image
//    {
//        $this->thumb = $thumb;
//
//        return $this;
//    }
//
//    public function getThumb(): ?string
//    {
//        return $this->thumb;
//    }
//
//    public function setSmall(?string $small): Image
//    {
//        $this->small = $small;
//
//        return $this;
//    }
//
//    public function getSmall(): ?string
//    {
//        return $this->small;
//    }
//
//    public function setMedium(?string $medium): Image
//    {
//        $this->medium = $medium;
//
//        return $this;
//    }
//
//    public function getMedium(): ?string
//    {
//        return $this->medium;
//    }
//
//    public function setLarge(?string $large): Image
//    {
//        $this->large = $large;
//
//        return $this;
//    }
//
//    public function getLarge(): ?string
//    {
//        return $this->large;
//    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function setPost(Post $post): Image
    {
        $this->post = $post;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Image
    {
        $this->user = $user;
        return $this;
    }
}
