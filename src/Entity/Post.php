<?php
namespace Blog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity
 */
class Post
{

    const STATUS_DRAFT = 'Brouillon';
    const STATUS_PUBLISHED = 'Publié';
    const STATUS_ARCHIVE = 'Archivé';
    const STATUS_DELETED = 'Supprimé';

    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $postId;

    /**
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private ?string $title;

    /**
     * @ORM\Column(name="chapo", type="text", length=65535, nullable=true)
     */
    private ?string $chapo;

    /**
     * @ORM\Column(name="content", type="text", length=4294967292, nullable=true)
     */
    private ?string $content;

    /**
     * @ORM\Column(name="creation_date_time", type="datetime")
     */
    private ?DateTime $creationDateTime;

    /**
     * @ORM\Column(name="last_update_date_time", type="datetime", nullable=true)
     */
    private ?DateTime $lastUpdateDateTime;

    /**
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private string $status;

    /**
     * @ORM\OneToMany(targetEntity="Blog\Entity\Image", mappedBy="post", cascade={"persist", "remove"})
     */
    private ?Collection $images;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     */
    private ?User $user;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private ?Collection $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->status = self::STATUS_DRAFT;
        $this->creationDateTime = new DateTime();
    }

    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getId(): int
    {
        return $this->postId;
    }

    public function setTitle(?string $title): Post
    {
        $this->title = $title;

        return $this;
    }

    public function getChapo(): ?string
    {
        return $this->chapo;
    }

    public function setChapo(?string $chapo): Post
    {
        $this->chapo = $chapo;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setContent(?string $content): Post
    {
        $this->content = $content;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setCreationDateTime(?DateTime $creationDateTime): Post
    {
        $this->creationDateTime = $creationDateTime;

        return $this;
    }

    public function getCreationDateTime(): ?DateTime
    {
        return $this->creationDateTime;
    }

    public function setLastUpdateDateTime(?DateTime $lastUpdateDateTime): ?Post
    {
        $this->lastUpdateDateTime = $lastUpdateDateTime;

        return $this;
    }

    public function getLastUpdateDateTime(): ?DateTime
    {
        return $this->lastUpdateDateTime;
    }

    public function setStatus(string $status): Post
    {
        if (!in_array(
            $status,
            array(self::STATUS_DRAFT, self::STATUS_PUBLISHED, self::STATUS_ARCHIVE, self::STATUS_DELETED)
        )) {
            throw new \InvalidArgumentException("Statut invalide");
        }
        $this->status= $status;
        return $this;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(?Image $image): Post
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }
        return $this;
    }

    public function removeImage(?Image $image) : Post
    {
        if ($this->images->contains($image)) {
            $this->images->remove($image) ;
        }
        return $this;
    }

    public function setUser(?User $user): Post
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getComments() : Collection
    {
        return $this->comments;
    }

    public function addComment(?Comment $comment) : Post
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
        }
        return $this;
    }

    public function removeComment(?Comment $comment) : Post
    {
        if ($this->comments->contains($comment)) {
            $this->comments->remove($comment) ;
        }
        return $this;
    }

    public function getHiglight(): ?Image
    {
        $higlight = null;
        foreach ($this->getImages() as $image) {
            if ($image->isHighlight() == true) {
                $higlight=$image;
            }
        }
        return $higlight;
    }
}
