<?php
namespace Blog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 * @package Blog\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $userId;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private string $password;

    /**
     * @ORM\Column(name="last_name", type="string", length=45, nullable=true)
     */
    private ?string $lastName = null;

    /**
     * @ORM\Column(name="first_name", type="string", length=45, nullable=true)
     */
    private ?string $firstName = null;

    /**
     * @ORM\Column(name="user_name", type="string", length=45)
     */
    private ?string $userName;

    /**
     * @ORM\Column(name="email", type="string", length=45)
     */
    private string $email ;

    /**
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private ?string $title = null;

    /**
     *
     * @ORM\Column(name="subscription_date", type="datetime", nullable=true)
     */
    private ?DateTime $subscriptionDate;

    /**
     *
     * @ORM\Column(name="email_validated", type="boolean")
     */
    private bool $emailValidated;

    /**
     *
     * @ORM\Column(name="activated", type="boolean")
     */
    private bool $activated;

    /**
     *  @ORM\Column(name="profile_pic", type="string", length=255, nullable=true)
     */
    private ?string $profilePic = null;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user")
     */
    private Collection $images;

    /**

     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role", referencedColumnName="id")
     * })
     */
    private ?Role $role;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user", cascade={"persist", "remove"})
     */
    private Collection $posts;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user", cascade={"persist", "remove"})
     */
    private Collection $comments;


    public function __construct()
    {
        $this->subscriptionDate=new DateTime();
        $this->images=new ArrayCollection();
        $this->posts=new ArrayCollection();
        $this->activated = false;
        $this->emailValidated = false;
    }

    public function setDefault(string $userName, string $email, Role $role): User
    {
        $this->role=$role;
        $this->userName=$userName;
        $this->email=$email;
        return $this;
    }

    public function makePassword(string $password)
    {
        return $this->setPassword(password_hash($password, PASSWORD_BCRYPT)) ;
    }

    public function getId(): int
    {
        return $this->userId;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }


    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPosts(): ?ArrayCollection
    {
        return $this->posts;
    }

    public function addPost(?Post $post): User
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
        }
        return $this;
    }

    public function removePost(?Post $post): User
    {
        if ($this->posts->contains($post)) {
            $this->posts->remove($post) ;
        }
        return $this;
    }

    public function getProfilePic(): ?string
    {
        return $this->profilePic;
    }

    public function setProfilePic(?string $profilePic): User
    {
        $this->profilePic = $profilePic;
        return $this;
    }


    public function getImages(): ?Collection
    {
        return $this->images;
    }


    public function addImage(?Image $image): User
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }
        return $this;
    }

    public function removeImage(?Image $image): User
    {
        if ($this->images->contains($image)) {
            $this->images->remove($image) ;
        }
        return $this;
    }

    public function getComments(): ?Collection
    {
        return $this->comments;
    }

    public function addComment(?Comment $comment): User
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
        }
        return $this;
    }

    public function removeComment(?Comment $comment): User
    {
        if ($this->comments->contains($comment)) {
            $this->comments->remove($comment) ;
        }
        return $this;
    }

    public function setRole(Role $role): User
    {
        $this->role = $role;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(?string $userName): User
    {
        $this->userName = $userName;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): User
    {
        $this->title = $title;
        return $this;
    }

    public function getSubscriptionDate(): ?DateTime
    {
        return $this->subscriptionDate;
    }

    public function setSubscriptionDate(DateTime $subscriptionDate): User
    {
        $this->subscriptionDate = $subscriptionDate;
        return $this;
    }

    public function isEmailValidated(): bool
    {
        return $this->emailValidated;
    }

    public function setEmailValidated(bool $emailValidated): User
    {
        $this->emailValidated = $emailValidated;
        return $this;
    }

    public function isActivated(): bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): User
    {
        $this->activated = $activated;
        return $this;
    }
}
