<?php
namespace Blog\Orm ;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

class MyEntityManager
{
    protected EntityManagerInterface $entityManager;

    public function __construct()
    {
        $entitiesPath = [ __DIR__."./../Entity"];
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $ormConfig = Setup::createAnnotationMetadataConfiguration(
            $entitiesPath,
            $isDevMode,
            $proxyDir,
            $cache,
            $useSimpleAnnotationReader
        );
        try {
            $this->entityManager = EntityManager::create($this->getDB(), $ormConfig);
        } catch (ORMException $e) {
            return $e->getMessage() ;
        }
        return $this->entityManager;
    }

    private function getDB()
    {
        $siteConfig= yaml_parse_file(__DIR__ . '/../../config/orm.local.yml');
        return $siteConfig['database'];
    }

    public function getCli()
    {
        return ConsoleRunner::createHelperSet($this->entityManager);
    }

    public function getConnection()
    {
        return $this->entityManager->getConnection();
    }

    public function getRepository($className)
    {
        return $this->entityManager->getRepository($className);
    }

    public function persist($object)
    {
        try {
            $this->entityManager->persist($object);
        } catch (ORMException $e) {
        }
    }

    public function flush()
    {
        try {
            $this->entityManager->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
    }

    /**
     * @param string $className
     * @return Mapping\ClassMetadata
     */
    public function getClassMetadata($className)
    {
        return $this->entityManager->getClassMetadata($className);
    }

    public function getMetadataFactory()
    {
        return $this->entityManager->getMetadataFactory();
    }

    public function getCache()
    {
        return $this->entityManager->getCache();
    }

    public function getExpressionBuilder()
    {
        return $this->entityManager->getExpressionBuilder();
    }

    public function getEventManager()
    {
        return $this->entityManager->getEventManager();
    }

    public function getConfiguration()
    {
        return $this->entityManager->getConfiguration();
    }

    public function remove($object)
    {
        try {
            $this->entityManager->remove($object);
        } catch (ORMException $e) {
        }
    }
}
