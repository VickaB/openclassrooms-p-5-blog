<?php


namespace Blog\Classes;

class Config
{
    protected array $siteConfig ;
//    protected array $ormConfig;


    public function siteConfig()
    {
        return $this->siteConfig = yaml_parse_file(__DIR__ . '/../../config/parameters.yml');
    }
//    protected function ormConfig()
//    {
//        return $this->ormConfig = yaml_parse_file(__DIR__ . '/../../config/orm.yml');
//    }
}
